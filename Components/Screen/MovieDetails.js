import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

let Image_Http_URL = { uri: 'https://reactnativecode.com/wp-content/uploads/2017/05/react_thumb_install.png' };


const MovieDetails = ({ navigation, route: { params } }) => {
    let { data } = params;
    // alert(JSON.stringify(data))
    return (
        <View style={{ flex: 0.5, justifyContent: 'center', alignItems: "center" }}>
            <Text>{data.title}</Text>
            <Text>{data.price}</Text>
            <View style={{ height: 100, width: 200, backgroundColor: "#ececec" }}>
                <Image source={{ uri: data.image }} style={{ height: 200, width: "100%" }} />
            </View>
        </View>
    );
}
export default MovieDetails;
